<?php

namespace app\interfaces;

use app\models\CertificateForm;

interface CertificateInterface
{
    /**
     * Get certificate by id
     *
     * @param int $id
     */
    public function getCertificate(string $id): CertificateForm|null;

    /**
     * Save certificate
     */
    public function createCertificate(CertificateForm $form): bool;
}
