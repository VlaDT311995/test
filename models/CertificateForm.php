<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * CertificateForm
 */
class CertificateForm extends Model
{
    public $id;
    public $courceName;
    public $studentName;
    public $completionDate;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id', 'courceName', 'studentName'], 'required'],
            [['id', 'courceName', 'studentName'], 'string'],
            [['completionDate'], function ($attribute) {
                if (is_int($this->$attribute)) {
                    return true;
                }
                if (empty($this->$attribute)) {
                    $this->$attribute = time();
                    return true;
                } else {
                    $this->$attribute = strtotime($this->$attribute);
                    return true;
                }
                return false;
            }],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Number of certificate',
            'courceName' => 'Cource name',
            'studentName' => 'Name of student',
            'completionDate' => 'Course end date',
        ];
    }
}
