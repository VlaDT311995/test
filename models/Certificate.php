<?php

namespace app\models;

use Yii;
use yii\base\BaseObject;
use app\models\CertificateForm;
use app\interfaces\CertificateInterface;

class Certificate extends BaseObject implements CertificateInterface
{
    /**
     * @inheritDoc
     */
    public function getCertificate(string $id): CertificateForm|null
    {
        $result = Yii::$app->cache->get($id);
        if (empty($result)) {
            return null;
        }
        return Yii::createObject('app\models\CertificateForm', [$result]);
    }
    
    /**
     * @inheritDoc
     */
    public function createCertificate(CertificateForm $form): bool
    {
        if (!$form->validate()) {
            return false;
        }
        if (!empty($this->getCertificate($form->id))) {
            $form->addError('id', 'Certificate with number "'.$form->id.'" already exists');
            return false;
        }
        Yii::$app->cache->set($form->id, $form->getAttributes());
        return true;
    }

}
