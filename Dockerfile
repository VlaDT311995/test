FROM nginx

#use nginx conf
COPY ./docker.nginx /etc/nginx/conf.d/default.conf
RUN echo "fastcgi_param SCRIPT_FILENAME \$document_root/\$fastcgi_script_name;" >> /etc/nginx/fastcgi_params

#installing requirements
RUN apt update && apt install -y git unzip php-cli php-intl curl php-mbstring php-curl php-dom php-gd php-zip wget

#copy proj to nginx dir, setting rights
COPY . /var/www/html

RUN chown www-data:www-data -R /var/www/html
#open the port
EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]