<?php

namespace app\overrides\controllers;

use Yii;
use yii\base\Model;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;
use yii\web\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * Ajax ActiveForm validation
     *
     * @param Model|null $model
     * @return void
     */
    public function ajaxValidation(Model $model = null)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = ActiveForm::validate($model);
            Yii::$app->end();
        }
    }
}