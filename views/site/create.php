<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\bootstrap4\ActiveForm;

$this->title = 'Create certificate';
?>
<div class="row">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <?php $form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]);?>
        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'courceName') ?>
        <?= $form->field($model, 'studentName') ?>
        <?= $form->field($model, 'completionDate')->widget(
            DatePicker::class,
            [
                'dateFormat' => 'yyyy-MM-dd',
                'options' => ['class' => 'form-control col-lg-6']
            ]
        ) ?>


        <?=Html::submitButton('Create', [
            'class' => 'btn btn-success',
        ])?>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-lg-2"></div>
</div>

