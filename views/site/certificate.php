<?php
?>
<div class="row">
    <h1>Certificate №<?=$form->id?></h1>
    <p>For successful completion of the course <b><?=$form->courceName?></b></p>
    <p>For the student <b><?=$form->studentName?></b></p>
    <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=<?=$qrUrl?>&choe=UTF-8" title="Link to Google.com" />
    <p><b><?=Yii::$app->formatter->asDate($form->completionDate)?></b></p>
</div>

