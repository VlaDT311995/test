# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Its test repository

### How do I get set up? ###

* Works with linux
* Clone this project
* Install composer if it is not installed and run composer install
* Install docker if it is not installed
* Change docker.nginx file if it is necessary
* Add server name in hosts (test.dv and www.test.dv by default)
* Run "docker-compose up"