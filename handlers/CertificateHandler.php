<?php

namespace app\handlers;

use Yii;
use yii\base\BaseObject;
use app\models\CertificateForm;
use app\interfaces\CertificateInterface;

class CertificateHandler extends BaseObject
{
    /**
     * @var CertificateInterface
     */
    protected $certificateModel;

    /**
     * @inheritDoc
     */
    public function __construct(CertificateInterface $certificateModel)
    {
        $this->certificateModel = $certificateModel;
    }

    /**
     * Save certificate to storage
     * 
     * @param CertificateForm $form
     * @return boolean
     */
    public function createCertificate(CertificateForm $form): bool
    {
        return $this->certificateModel->createCertificate($form);
    }
    
    /**
     * Get certificate
     *
     * @param integer $id
     * @return void
     */
    public function getCertificate(string $id)
    {
        $form = $this->certificateModel->getCertificate($id);
        if (empty($form)) {
            return null;
        }
        return $form;
    }
}
