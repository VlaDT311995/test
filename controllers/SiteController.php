<?php

namespace app\controllers;

use app\models\CertificateForm;
use Yii;
use Mpdf\Mpdf;
use yii\helpers\Url;
use app\overrides\controllers\Controller;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Create certificate
     *
     * @return string
     */
    public function actionCreate()
    {
        $model = Yii::createObject('app\models\CertificateForm');
        if ($model->load(Yii::$app->request->post())) {
            $this->ajaxValidation($model);
            $handler = Yii::createObject('app\handlers\CertificateHandler');
            if ($handler->createCertificate($model)) {
                return $this->redirect(['certificate', 'id' => $model->id]);
            }
            if (!empty($model->getFirstErrors())) {
                Yii::$app->session->setFlash(
                    'error', 'Error occurred: '.implode(';', $model->getFirstErrors())
                );
            } else {
                Yii::$app->session->setFlash(
                    'error', 'An unknown error has occurred, please contact support'
                );
            }
            return $this->refresh();
        }

        return $this->render(
            'create',
            [
                'model' => $model
            ]
        );
    }

    /**
     * Get and output certificate
     *
     * @param string $id
     */
    public function actionCertificate(string $id, $type = self::EXPORT_TYPE_PDF)
    {
        $handler = Yii::createObject('app\handlers\CertificateHandler');
        $form = $handler->getCertificate($id);
        if (empty($form)) {
            Yii::$app->session->setFlash('error', 'Certificate not found');
            return $this->goHome();
        }
        $this->export(
            $this->renderPartial(
                'certificate',
                [
                    'form' => $form,
                    'qrUrl' => urlencode(Yii::$app->urlManager->createAbsoluteUrl($this->request->url)),
                ]
            ),
            $type
        );
    }

    const EXPORT_TYPE_PDF = 'pdf';

    /**
     * Export html to pdf
     *
     * @param string $view
     * @param string $type
     * @return void
     */
    public function export(string $html, string $type = self::EXPORT_TYPE_PDF)
    {
        switch ($type) {
            case self::EXPORT_TYPE_PDF:
                $mpdf = new \Mpdf\Mpdf();
                $mpdf->SetTitle("Pdf Certificate");
                $mpdf->WriteHTML($html);
                $mpdf->Output();
                break;
            default:
                Yii::$app->session->setFlash('error', 'Type is not supported');   
                return $this->goHome();
        }
    }
}
